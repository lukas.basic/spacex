import MainPage from './Pages/MainPage';
import { navItems } from './Utils/data';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import './App.css';

function App() {
  return (
    <div className="app-background app-color">
        <Router>
          <Switch>
            <Route exact path="/">
              <MainPage page=""/>
            </Route>
            {navItems.map((item) => {
              return  <Route path={"/" + item.toLocaleLowerCase()}>
                        <MainPage page={item}/>
                      </Route>
            })}
          </Switch>
        </Router>
    </div>
  );
}

export default App;
