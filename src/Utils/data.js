export const title = "SpaceX";

export const navItems = [
    "Crew",
    "Rockets",
    "Events"
];

export default { title, navItems };