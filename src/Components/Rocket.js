import React from 'react';

import '../Styles/Modals.css';

class Rocket extends React.Component {
    constructor(props) {
        super(props);
        this.setShow = props.setShow;
        this.rocket = props.rocket;
    }

    render() {
        return (
            <>
                <div className="overlay" onClick={() => this.setShow(false)}>
                    <div className="modal" onClick={(e) => e.stopPropagation()}>
                        <div className="modal-content">
                            <span className="name">{this.rocket.name}</span>
                            <span className="desc">{this.rocket.description}</span>
                            <div><a className="wiki" href={this.rocket.wikipedia}>See on wikipedia</a></div>
                            <div>
                                <a href={this.rocket.flickr_images[0]}>
                                    <img className="rocket" src={this.rocket.flickr_images[0]} alt={this.rocket.name} />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Rocket;