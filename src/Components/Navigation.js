import React, { useState } from 'react';
import Menu from './Menu';
import { useTransition, animated } from 'react-spring';

import '../Styles/Header.css';

function Navigation(props) {
    const [showMenu, setShowMenu] = useState(false);

    const maskTransitions = useTransition(showMenu, null, {
        from: { position: 'absolute', opacity: 0 },
        enter: { opacity: 1 },
        leave: { opacity: 0 },
    });

    const transitions = useTransition(showMenu, null, {
        from: { opacity: 0, transform: 'translateX(-100%)' },
        enter: { opacity: 1, transform: 'translateX(0%)' },
        leave: { opacity: 0, transform: 'translateX(-100%)' },
    });

    let setItems = props.setItems;

    return (
        <nav className="nav">
            <input type="checkbox" id="openmenu" class="hamburger-checkbox" checked={showMenu} />
  
            <div class="hamburger-icon" onClick={() => setShowMenu(!showMenu)}>
                <label for="openmenu" id="hamburger-label">
                <span></span>
                <span></span>
                <span></span>
                </label>    
            </div>

            {
                maskTransitions.map(({ item, key, props }) =>
                    item && 
                    <animated.div 
                        key={key} 
                        style={props}
                        className="menuMask"
                    >
                        
                        <div className="menuMask"
                            onClick={() => setShowMenu(false)}></div>
                    </animated.div>
                )
            }

            {
                transitions.map(({ item, key, props }) =>
                    item && 
                    <animated.div 
                        key={key} 
                        style={props}
                        className="menu"
                    >
                        <Menu setShow={setShowMenu} setItems={setItems}/>
                    </animated.div>
                )
            }
        </nav>
    );
}

export default Navigation;