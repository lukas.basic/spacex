import React from 'react';
import CrewMember from './CrewMember';

import '../App.css';

class Crew extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            modal: {},
            crew: []
        }
    }

    async componentDidMount() {        
        try {
          let res = await fetch('https://api.spacexdata.com/v4/crew');
          let result = await res.json();
    
          if (result) {
            this.setState({
                crew: result
            });
          }
        } catch (e) {
        }
    }

    setShowModal = e => {
        this.setState({
            showModal: e
        })
    }

    setModal = e => {
        this.setState({
            modal: e
        })
    }

    render() {
        return (
            <>
                <div className="items">
                    {this.state.crew.map((item) => {
                        return <div key={item.name}>
                                    <span className="btn"
                                        onClick={() => {this.setShowModal(true); this.setModal(item)}}
                                    >
                                        {item.name}
                                    </span>
                                </div>
                    })}
                </div>

                {this.state.showModal &&
                    <CrewMember setShow={this.setShowModal}
                                member={this.state.modal}
                    />
                }
            </>
        );
    }
}

export default Crew;