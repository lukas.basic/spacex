import React from 'react';

import '../App.css'

function Footer() {
    return (
        <footer className="footer app-color">
            &copy; b2match Frontend Programming Test - Lukas Bašić
        </footer>
    );
}

export default Footer;
