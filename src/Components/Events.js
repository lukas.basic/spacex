import React from 'react';
import Event from './Event';

import '..//App.css';

class Events extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            modal: {},
            events: []
        }
    }

    async componentDidMount() {        
        try {
          let res = await fetch('https://api.spacexdata.com/v4/history');
          let result = await res.json();
    
          if (result) {
            this.setState({
                events: result
            });
          }
        } catch (e) {
        }
    }

    setShowModal = e => {
        this.setState({
            showModal: e
        })
    }

    setModal = e => {
        this.setState({
            modal: e
        })
    }

    render() {
        return (
            <>
                <div className="items">
                    {this.state.events.map((item) => {
                        return <div key={item.title}>
                                    <span className="btn"
                                        onClick={() => {this.setShowModal(true); this.setModal(item)}}
                                    >
                                        {item.title}
                                    </span>
                                </div>
                    })}
                </div>

                {this.state.showModal &&
                    <Event setShow={this.setShowModal}
                                event={this.state.modal}
                    />
                }
            </>
        );
    }
}

export default Events;