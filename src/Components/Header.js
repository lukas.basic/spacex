import React from 'react';
import Navigation from './Navigation';
import { Link } from "react-router-dom";
import { title, navItems } from '../Utils/data';

import '../Styles/Header.css';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.setItems = props.setItems;
        this.state = {
            windowWidth: 0,
            windowHeight: 0,
            collapsedHeader: false
        };
        this.updateDimensions = this.updateDimensions.bind(this);
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    updateDimensions() {
        let windowWidth = typeof window !== "undefined" ? window.innerWidth : 0;
        let windowHeight = typeof window !== "undefined" ? window.innerHeight : 0;
        let collapsedHeader = windowWidth < 768;

        this.setState({ windowWidth, windowHeight, collapsedHeader });
    }

    render() {
        return (
            <header className="header app-color">
                {!this.state.collapsedHeader &&
                    <div className="header-items">
                        
                        <Link to="/">
                            <div className="title btn hoverable"
                                onClick={() => this.setItems("")}
                            >
                                {title}
                            </div>
                        </Link>
                        
                        <div className="menu-items">
                            {navItems.map((item) => {
                                return  <Link to={"/" + item.toLocaleLowerCase()}>
                                            <div key={item}
                                                className="btn hoverable"
                                                onClick={() => this.setItems(item)}
                                            >
                                                {item.toLocaleUpperCase()}
                                            </div>
                                        </Link>
                            })}
                        </div>
                    </div>
                }

                {this.state.collapsedHeader &&
                    <div className="collapsed">
                        <div className="hamburgerIcon">
                            <Navigation setItems={this.setItems}/>
                        </div>
                        <Link to="/">
                            <div className="titleCollapsed btn"
                                onClick={() => this.setItems("")}
                            >
                                {title}
                            </div>
                        </Link>
                    </div>
                }
            </header>
        );
    }
}

export default Header;
