import React from 'react';

import '../Styles/Modals.css';

class CrewMember extends React.Component {
    constructor(props) {
        super(props);
        this.setShow = props.setShow;
        this.member = props.member;
    }

    render() {
        return (
            <>
                <div className="overlay" onClick={() => this.setShow(false)}>
                    <div className="modal" onClick={(e) => e.stopPropagation()}>
                        <div className="modal-content">
                            <span className="name">{this.member.name}</span>
                            <span className="agency">Agency: {this.member.agency}</span>
                            <div><a className="wiki" href={this.member.wikipedia}>See on wikipedia</a></div>
                            <div>
                                <a href={this.member.image}>
                                    <img className="member" src={this.member.image} alt={this.member.name} />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default CrewMember;