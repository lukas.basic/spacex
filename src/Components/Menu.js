import React from 'react';
import { Link } from "react-router-dom";
import { title, navItems } from '../Utils/data';

import '../Styles/Header.css';

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.setShow = props.setShow;
        this.setItems = props.setItems;
    }
    render() {
        return (
            <>
                <div>
                    <div className="menu-title">
                        <Link to="/">
                            <div className="title btn"
                                onClick={() => {this.setItems(""); this.setShow(false)}}
                            >
                                {title}
                            </div>
                        </Link>
                    </div>
                    
                    <div className="items">
                        {navItems.map((item) => {
                                return  <Link to={"/" + item.toLocaleLowerCase()}>
                                            <div key={item}
                                                className="btn"
                                                onClick={() => {this.setItems(item); this.setShow(false)}}
                                            >
                                                {item}
                                            </div>
                                        </Link>
                        })}
                    </div>
                </div>
            </>
        );
    }
}

export default Menu;