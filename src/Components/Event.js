import React from 'react';

import '../Styles/Modals.css';

class Event extends React.Component {
    constructor(props) {
        super(props);
        this.setShow = props.setShow;
        this.event = props.event;
        this.state = {
            date: ""
        }
    }

    async componentDidMount() {
        let date_index = this.event.event_date_utc.indexOf('T');
        let date = this.event.event_date_utc.substring(0, date_index)
        this.setState({
            date: date
        })
    }

    render() {
        return (
            <>
                <div className="overlay" onClick={() => this.setShow(false)}>
                    <div className="modal" onClick={(e) => e.stopPropagation()}>
                        <div className="modal-content">
                            <span className="name">{this.event.title}</span>
                            <time datetime={this.event.event_date_utc}>{this.state.date}</time>
                            <span className="desc">{this.event.details}</span>
                            <div><a className="wiki" href={this.event.links.article}>See article</a></div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Event;