import React from 'react';
import Rocket from './Rocket';

import '../App.css';

class Rockets extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            modal: {},
            rockets: []
        }
    }

    async componentDidMount() {        
        try {
          let res = await fetch('https://api.spacexdata.com/v4/rockets');
          let result = await res.json();
    
          if (result) {
            this.setState({
                rockets: result
            });
          }
        } catch (e) {
        }
    }

    setShowModal = e => {
        this.setState({
            showModal: e
        })
    }

    setModal = e => {
        this.setState({
            modal: e
        })
    }

    render() {
        return (
            <>
                <div className="items">
                    {this.state.rockets.map((item) => {
                        return <div key={item.name}>
                                    <span className="btn"
                                        onClick={() => {this.setShowModal(true); this.setModal(item)}}
                                    >
                                        {item.name}
                                    </span>
                                </div>
                    })}
                </div>

                {this.state.showModal &&
                    <Rocket setShow={this.setShowModal}
                                rocket={this.state.modal}
                    />
                }
            </>
        );
    }
}

export default Rockets;