import React from 'react';
import {withRouter} from 'react-router-dom';
import Crew from '../Components/Crew';
import Events from '../Components/Events';
import Rockets from '../Components/Rockets';
import Header from '../Components/Header';
import Footer from '../Components/Footer';

import '../App.css';

class MainPage extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            page: props.page
        }
    }

    page = () => {
        alert(this.page)
    }

    setItems = e => {
        this.setState({
            page: e
        })
    }

    render() {
        if (this.state.page !== "")
            return (
                <>
                    <Header setItems={this.setItems}/>

                    <div className="space"></div>
                    <div className="title">{this.state.page.toLocaleUpperCase()}</div>
                    
                    {this.state.page === "Crew" &&
                        <Crew />
                    }
                    {this.state.page === "Rockets" &&
                        <Rockets />
                    }
                    {this.state.page === "Events" &&
                        <Events />
                    }

                    <Footer />
                </>
            );
        else
            return (
                <>
                    <Header setItems={this.setItems}/>
                    
                    <div className="items">
                        <span>Welcome to SpaceX!</span>
                    </div>

                    <Footer />
                </>
            );
    }
}

export default withRouter(MainPage);